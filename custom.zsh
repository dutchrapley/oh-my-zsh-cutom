PATH=/usr/local/bin:/usr/local/sbin:~/bin:$PATH
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

alias reloaddns='sudo killall -HUP mDNSResponder'
alias play='ansible-playbook'
alias vault='ansible-vault'
alias encrypt='ansible-vault encrypt'
alias decrypt='ansible-vault decrypt'
alias visualvm='/Users/drapley/Applications/visualvm_139/bin/visualvm &'
alias rake="noglob bundle exec rake"
alias roulette='[ $(( $RANDOM % 6 )) -eq 0 ] && echo "Bang!" || echo "click & spin"'
alias play_ghost_stories='for run in {1..75}; do [[ $(( $RANDOM % 6 )) -eq 0 && $(( $RANDOM % 6 )) -eq 0 && $(( $RANDOM % 6 )) -eq 0 && $(( $RANDOM % 6 )) -eq 0 && $(( $RANDOM % 6 )) -eq 0 ]] && echo "You Won" ; done | grep "You Won" || echo "You Lost"'
